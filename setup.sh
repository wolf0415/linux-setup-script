#!/bin/bash


if [[ $(id -u) != 0  ]];
	echo "This script required to run under administrative privileges"
	exit 1;



sudo apt update && sudo apt upgrade

tools_used=("vim" "tree" "git" "gdb" "g++" "gcc" "radare2" "python3-pip" "mousepad" "python3-dev" "git" "libssl-dev" "libffi-dev" "build-essential")

for tool in ${tools_used[@]}
do
	sudo apt install -y $tool
done

snap --version > /dev/null
if [ $?=0 ];
then 
	sudo snap install --classic code 
else
	snap apt install snapd
	sudo snap install --classic code
fi


sudo apt-get install openjdk-11-jdk

# wget https://github.com/NationalSecurityAgency/ghidra/archive/refs/heads/master.zip
# unzip ghidra-master
# cd ghidra-master
# gradle -I gradle/support/fetchDependencies.gradle init
# gradle buildGhidra

echo "set number" > ~/.vimrc
echo "set autoindent" >> ~/.vimrc
echo "set tabstop=4" >> ~/.vimrc
echo "set shiftwidth=4" >> ~/.vimrc
echo "set softtabstop=4" >> ~/.vimrc
echo "set noswapfile" >> ~/.vimrc